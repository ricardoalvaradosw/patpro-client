var listaProductos = [
    {
        nombre: 'Tv Samsung',
        codigo: 'PRO001',
        cantidad: '1',
        precio: 2500,
        estado: 2,
        fechaCompra: '01/05/2019',
        fechaEntrega: '05/05/2019',
        ciudadEntrega: 'Piura',
        domicilio: 'Urb. Jardines Avifap G 10',
        numeroReferencia: '945741615',
        responsable1: 'Jecri Do Santos',
        responsable2: 'Renzo Do Santos'
    },
    {
        nombre: 'Laptop Asus',
        codigo: 'PRO002',
        cantidad: '1',
        precio: 2500,
        estado: 0,
        fechaCompra: '01/05/2019',
        fechaEntrega: '05/05/2019',
        ciudadEntrega: 'Trujillo',
        domicilio: 'Urb. Jardines Avifap G 10',
        numeroReferencia: '945741615',
        responsable1: 'Jecri Do Santos',
        responsable2: 'Renzo Do Santos'
    },
    {
        nombre: 'Moto G5 Plus S',
        codigo: 'PRO003',
        cantidad: '1',
        precio: 1200,
        estado: 4,
        fechaCompra: '01/05/2019',
        fechaEntrega: '05/05/2019',
        ciudadEntrega: 'Sullana',
        domicilio: 'Urb. Jardines Avifap G 10',
        numeroReferencia: '945741615',
        responsable1: 'Jecri Do Santos',
        responsable2: 'Renzo Do Santos'
    },
    {
        nombre: 'Moto G5 Plus S',
        codigo: 'PRO004',
        cantidad: '1',
        precio: 1200,
        estado: 3,
        fechaCompra: '01/05/2019',
        fechaEntrega: '05/05/2019',
        ciudadEntrega: 'Sullana',
        domicilio: 'Urb. Jardines Avifap G 10',
        numeroReferencia: '945741615',
        responsable1: 'Jecri Do Santos',
        responsable2: 'Renzo Do Santos'
    },
    {
        nombre: 'Moto G5 Plus S',
        codigo: 'PRO005',
        cantidad: '1',
        precio: 1200,
        estado: 0,
        fechaCompra: '01/05/2019',
        fechaEntrega: '05/05/2019',
        ciudadEntrega: 'Sullana',
        domicilio: 'Urb. Jardines Avifap G 10',
        numeroReferencia: '945741615',
        responsable1: 'Jecri Do Santos',
        responsable2: 'Renzo Do Santos'
    },
    {
        nombre: 'Moto G5 Plus S',
        codigo: 'PRO006',
        cantidad: '1',
        precio: 1200,
        estado: 0,
        fechaCompra: '01/05/2019',
        fechaEntrega: '05/05/2019',
        ciudadEntrega: 'Sullana',
        domicilio: 'Urb. Jardines Avifap G 10',
        numeroReferencia: '945741615',
        responsable1: 'Jecri Do Santos',
        responsable2: 'Renzo Do Santos'
    }
];
var totalMonto = 0;
var estados = [
    'Pendiente',
    'En salida',
    'Enviado',
    'Recepcionado',
    'A domicilio',
    'Entregado'
]

$(document).ready(() => {
    inicializarProdcutos();
});

function inicializarProdcutos() {
    var data = listaProductos;
    // var data = [];
    if (data.length == 0) {
        $('#productosEncontrados').hide();
        $('#productosNoEncontrados').show();
        totalMonto = 0;
    } else {
        $('#productosEncontrados').show();
        $('#productosNoEncontrados').hide();
        $.each(data, function (key, value) {
            totalMonto+= value.precio;
            $('#contenidoTabla').append(
                '<tr><th>' + key +
                '</th><td>' + value.nombre +
                '</td><td>' + value.codigo +
                '</td><td>' + value.cantidad +
                '</td><td>' + value.precio +
                '</td><td>' + estados[value.estado] +
                '</td><td>' + value.fechaCompra +
                '</td><td>' + value.fechaEntrega +
                '</td><td style="text-align: center;"><button type="button" id="btnVer'+key+'" onClick="mostrarDetalle(' + key + ')"  data-toggle="modal" data-target="#exampleModal" class="btn btn-info btn-sm">Ver</button></td></tr>');
        });
        $('#totalMonto').val(totalMonto);
        $('#totalProductos').val(data.length);
    }

}

function mostrarDetalle(key) {
    var producto = listaProductos[key];
    $('#inputNombre').val(producto.nombre);
    $('#inputCodigo').val(producto.codigo);
    $('#inputCantidad').val(producto.cantidad);
    $('#inputPrecio').val(producto.precio);
    $('#inputEstado').val(estados[producto.estado]);
    $('#inputFechaCompra').val(producto.fechaCompra);
    $('#inputFechaEntrega').val(producto.fechaEntrega);
    $('#inputCiudad').val(producto.ciudadEntrega);
    $('#inputDireccionEntrega').val(producto.domicilio);
    $('#inputNumeroReferencia').val(producto.numeroReferencia);
    $('#inputResponsable1').val(producto.responsable1);
    $('#inputResponsable2').val(producto.responsable2);

    var cadenaEstados = '';
    $.each(estados, function (key, value) {
        if (key <= producto.estado) {
            cadenaEstados += '<li class="active">' + value + '</li>'
        } else {
            cadenaEstados += '<li>' + value + '</li>'
        }
    });
    $('#lineaEstados').html(cadenaEstados);
}
