//declaracion de variables globales
valor = "";
valorGeneral = 7500;
rangeGeneral = 100;
valorToken = 123456;
//Carga inicial
$(document).ready(() => {
    iniciar();
    $('#btnVerCriterios').click(() => {
        $('#listaCriterios').slideToggle();
    });
    $('input[name="cobertura"]').on('change', function (e) {
        e.stopPropagation();
        valor = $(this).val();
        if (valor == 'M') {
            iniciar();
            $("#divPagoMontos").show();
            $("#divPagoMontos").css("pointer-events", "auto");
        } else if (valor == 'P') {
            iniciar();
            $("#divPagoPorcentajes").show();
            $("#divPagoPorcentajes").css("pointer-events", "auto");
        }
        $("#divComprar").show();
    });

    //Validar solo numeros en las cajas de texto
    $("#txtPayPal").keydown(function (event) {
        if (validarSoloNumeros(event)) {
            event.preventDefault();
        }
    });

    $("#txtVisa").keydown(function (event) {
        if (validarSoloNumeros(event)) {
            event.preventDefault();
        }
    });

    $("#txtMasterCard").keydown(function (event) {
        if (validarSoloNumeros(event)) {
            event.preventDefault();
        }
    });

    $("#txtBcp").keydown(function (event) {
        if (validarSoloNumeros(event)) {
            event.preventDefault();
        }
    });

    $("#txtBbva").keydown(function (event) {
        if (validarSoloNumeros(event)) {
            event.preventDefault();
        }
    });

    $("#txtCmac").keydown(function (event) {
        if (validarSoloNumeros(event)) {
            event.preventDefault();
        }
    });

});

function iniciar() {
    // Ocultamos los card
    $("#divToken").hide();
    $("#divComprar").hide();
    $("#divPagoPorcentajes").hide();
    $("#divPagoMontos").hide();

    //Quitamos la seleccion de los check
    $("#chkPaypal").prop("checked", false);
    $("#chk1Paypal").prop("checked", false);
    $("#chkVisa").prop("checked", false);
    $("#chk1Visa").prop("checked", false);
    $("#chkMasterCard").prop("checked", false);
    $("#chk1MasterCard").prop("checked", false);
    $("#chkBcp").prop("checked", false);
    $("#chk1Bcp").prop("checked", false);
    $("#chkBbva").prop("checked", false);
    $("#chk1Bbva").prop("checked", false);
    $("#chkCmac").prop("checked", false);
    $("#chk1Cmac").prop("checked", false);

    //Desabilitamos y reiniciamos las cajas de texto
    $("#txtPaypal").prop("disabled", true);
    $("#txtPaypal").val("");
    $("#txtVisa").prop("disabled", true);
    $("#txtVisa").val("");
    $("#txtMasterCard").prop("disabled", true);
    $("#txtMasterCard").val("");
    $("#txtBcp").prop("disabled", true);
    $("#txtBcp").val("");
    $("#txtBbva").prop("disabled", true);
    $("#txtBbva").val("");
    $("#txtCmac").prop("disabled", true);
    $("#txtCmac").val("");

    $("#txtTotalPayPal").val("");
    $("#txtTotalTarjeta").val("");
    $("#txtTotalTransferencia").val("");
    $("#txtToken").val("");

    //Desabilitamos y reiniciamos los slider
    $("#rangePaypal").val("0");
    $("#rangePaypal").prop("disabled", true);
    $("#rangeVisa").val("0");
    $("#rangeVisa").prop("disabled", true);
    $("#rangeMasterCard").val("0");
    $("#rangeMasterCard").prop("disabled", true);
    $("#rangeBcp").val("0");
    $("#rangeBcp").prop("disabled", true);
    $("#rangeBbva").val("0");
    $("#rangeBbva").prop("disabled", true);
    $("#rangeCmac").val("0");
    $("#rangeCmac").prop("disabled", true);
    $("#rangeTotalPaypal").val("0");
    $("#rangeTotalTarjeta").val("0");
    $("#rangeTotalTransferencia").val("0");

    //Botones
    $("#btnGenerarCompra").prop("disabled", true);
    $("#btnGenerarCompra").css("backgroundColor", "silver");
    $("#btnGenerarCompra").css("borderColor", "silver");

    $("#btnVerificarCompra").removeAttr("style");
    $("#btnVerificarCompra").prop("disabled", false);

    //Ocultamos mensajes
    $("#divCompraExitosa").hide();
    $("#divVerificacionError").hide();
    $("#divGeneracionExitosa").hide();
    $("#divCompraError").hide();

}


function validarSoloNumeros(event) {
    var rpta = false;
    if (event.shiftKey) {
        rpta = true;
    }
    if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 39 || event.keyCode == 37) {
        rpta = false;
    } else {
        if (event.keyCode < 95) {
            if (event.keyCode < 48 || event.keyCode > 57) {
                rpta = true;
            }
        } else {
            if (event.keyCode < 96 || event.keyCode > 105) {
                rpta = true;
            }
        }
    }
    return rpta;
}

function verificarComponente(id) {
    var idTxt = id.replace("chk", "txt");
    var idRange = id.replace("chk1", "range");
    if ($("#" + id).is(':checked')) {
        if (valor == 'M') {
            $("#" + idTxt).prop("disabled", false);
            $('#' + idTxt).focus();
        }
        else if (valor == 'P') {
            $("#" + idRange).prop("disabled", false);
        }
    }
    else {
        if (valor == 'M') {
            $("#" + idTxt).prop("disabled", true);
            $("#" + idTxt).val("");
        }
        else if (valor == 'P') {
            $("#" + idRange).prop("disabled", true);
            $("#" + idRange).val("0");
        }
    }
}

function verificarCompra() {
    $("#divVerificacionError").hide();
    if (valor == 'M') {
        verificarMontos();
    } else if (valor == 'P') {
        verificarPorcentajes();
    }
}

function verificarMontos() {

    var totalPaypal = nvl($("#txtPaypal").val(), 0);
    var totalTarjeta = nvl($("#txtVisa").val(), 0) + nvl($("#txtMasterCard").val(), 0);
    var totalTransferencia = nvl($("#txtBcp").val(), 0) + nvl($("#txtBbva").val(), 0) + nvl($("#txtCmac").val(), 0);

    var totalGeneral = totalPaypal + totalTarjeta + totalTransferencia;

    $("#txtTotalPayPal").val(totalPaypal);
    $("#txtTotalTarjeta").val(totalTarjeta);
    $("#txtTotalTransferencia").val(totalTransferencia);

    if (totalGeneral == valorGeneral) {
        $("#btnGenerarCompra").removeAttr("style");
        $("#btnGenerarCompra").prop("disabled", false);

        $("#btnVerificarCompra").prop("disabled", true);
        $("#btnVerificarCompra").css("backgroundColor", "silver");
        $("#btnVerificarCompra").css("borderColor", "silver");

        $("#divPagoMontos").css("pointer-events", "none");

    } else {
        $("#divVerificacionError").show();
    }

}

function verificarPorcentajes() {

    var totalPaypal = nvl($("#rangePaypal").val(), 0);
    var totalTarjeta = nvl($("#rangeVisa").val(), 0) + nvl($("#rangeMasterCard").val(), 0);
    var totalTransferencia = nvl($("#rangeBcp").val(), 0) + nvl($("#rangeBbva").val(), 0) + nvl($("#rangeCmac").val(), 0);
    console.log("totalPaypal " + totalPaypal);
    var totalGeneral = totalPaypal + totalTarjeta + totalTransferencia;
    console.log("totalGeneral " + totalGeneral);
    $("#rangeTotalPaypal").val(totalPaypal);
    $("#rangeTotalTarjeta").val(totalTarjeta);
    $("#rangeTotalTransferencia").val(totalTransferencia);

    if (totalGeneral == rangeGeneral) {
        $("#btnGenerarCompra").removeAttr("style");
        $("#btnGenerarCompra").prop("disabled", false);

        $("#btnVerificarCompra").prop("disabled", true);
        $("#btnVerificarCompra").css("backgroundColor", "silver");
        $("#btnVerificarCompra").css("borderColor", "silver");

        $("#divPagoPorcentajes").css("pointer-events", "none");
    } else {
        $("#divVerificacionError").show();
    }

}

function nvl(valorReal, valorDefecto) {
    if (valorReal == "") {
        valorReal = parseInt(valorDefecto);
    } else {
        valorReal = parseInt(valorReal);
    }
    return valorReal;
}

function generarCompra() {
    $("#divCompraError").hide();
    $("#divToken").show();
    $("#txtToken").val("");
    $("#txtToken").focus();
    $("#btnValidarToken").removeAttr("style");
    $("#btnValidarToken").prop("disabled", false);
    $("#divGeneracionExitosa").show();
}

function cancelarCompra() {
    iniciar();
    $("#radioMonto").prop("checked", false);
    $("#radioPorcentaje").prop("checked", false);
}

function validarToken() {
    var tokenPantalla = $("#txtToken").val();
    if (tokenPantalla == valorToken) {
        $("#divCompraExitosa").show();
    } else {
        $("#divCompraError").show();
        $("#btnValidarToken").prop("disabled", true);
        $("#btnValidarToken").css("backgroundColor", "silver");
        $("#btnValidarToken").css("borderColor", "silver");
    }
}
