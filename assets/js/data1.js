/*<option value="1">Polos</option>
<option value="2">Camisas</option>
<option value="3">Zapatillas</option>
<option value="4">Zapatos</option>
<option value="5">Vestidos</option>*/

var productos = [
    { codigo: '1', categoriaId: '4', categoria: 'Casacas', nombre: 'Casaca con 4 bolsillos Malabar', precio: 99.50, stock: 80, precioCompra: 45.0, cantidadVendida: 20 },
    { codigo: '2', categoriaId: '4', categoria: 'Casacas', nombre: 'Casaca acolchada con borados', precio: 101.50, stock: 60, precioCompra: 68.6, cantidadVendida: 48 },
    { codigo: '3', categoriaId: '4', categoria: 'Casacas', nombre: 'Casaca con capucha y cierres', precio: 119.50, stock: 80, precioCompra: 75.0, cantidadVendida: 44 },
    { codigo: '4', categoriaId: '1', categoria: 'Polos', nombre: 'Polo Manga corta Arizona', precio: 39.95, stock: 78, precioCompra: 9.0, cantidadVendida: 59 },
    { codigo: '5', categoriaId: '1', categoria: 'Polos', nombre: 'Polo Print Manga corta', precio: 39.95, stock: 80, precioCompra: 11.45, cantidadVendida: 60 },
    { codigo: '6', categoriaId: '1', categoria: 'Polos', nombre: 'Polo Manga Corta a rayas', precio: 24.50, stock: 90, precioCompra: 12.0, cantidadVendida: 65 },
    { codigo: '7', categoriaId: '1', categoria: 'Polos', nombre: 'Polo Cuello Camisero con bolsillo', precio: 49.50, stock: 78, precioCompra: 16.7, cantidadVendida: 20 },
    { codigo: '8', categoriaId: '1', categoria: 'Polos', nombre: 'Polo Cuello Piqué', precio: 39.98, stock: 79, precioCompra: 15.0, cantidadVendida: 45 },
    { codigo: '9', categoriaId: '3', categoria: 'Pantalones', nombre: 'Jean Corte Recto - The Royal', precio: 79.95, stock: 45, precioCompra: 40.0, cantidadVendida: 16 },
    { codigo: '10', categoriaId: '3', categoria: 'Pantalones', nombre: 'Jean Rasgado', precio: 99.95, stock: 70, precioCompra: 42.56, cantidadVendida: 33 },
    { codigo: '11', categoriaId: '3', categoria: 'Pantalones', nombre: 'Jean Focalizado SLIM', precio: 99.95, stock: 65, precioCompra: 49.95, cantidadVendida: 42 },
    { codigo: '12', categoriaId: '2', categoria: 'Camisas', nombre: 'Camisa Manga Larga Cuadros con Hoddie', precio: 99.50, stock: 60, precioCompra: 39.0, cantidadVendida: 28 },
    { codigo: '13', categoriaId: '2', categoria: 'Camisas', nombre: 'Camisa Print Slim Fit', precio: 79.50, stock: 77, precioCompra: 40.0, cantidadVendida: 33 },
    { codigo: '14', categoriaId: '2', categoria: 'Camisas', nombre: 'Camisa Minitprint Blanco', precio: 39.50, stock: 44, precioCompra: 29.99, cantidadVendida: 28 },
    { codigo: '15', categoriaId: '2', categoria: 'Camisas', nombre: 'Camisa Manga corta con estampado', precio: 49.50, stock: 39, precioCompra: 28.35, cantidadVendida: 20 },
];