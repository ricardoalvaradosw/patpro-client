var precioInicial = 0;
var precioFinal = 500;
$(document).ready(() => {
    console.log('Iniciando');
    $("#rPrecios").slider({
        tooltip: 'always'
    });
    $('#btnVerCriterios').click(() => {
        $('#listaCriterios').slideToggle();
    });
    $("#rPrecios").on("slide", function(slideEvt) {
        console.log(slideEvt.value);
        precioInicial = slideEvt.value[0];
        precioFinal = slideEvt.value[1];
        $('#minValue').html('S/. ' + slideEvt.value[0]);
        $('#maxValue').html('S/. ' + slideEvt.value[1]);
    });
    var start = moment().subtract(1, 'month').startOf('month');
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
    }

    $('#reportrange').daterangepicker({
        "locale": {
            "format": "DD/MM/YYYY",
            "separator": " - ",
            "applyLabel": "Aplicar",
            "cancelLabel": "Cancelar",
            "fromLabel": "Desde",
            "toLabel": "Hasta",
            "customRangeLabel": "Personalizado",
            "weekLabel": "S",
            "daysOfWeek": [
                "Do",
                "Lu",
                "Ma",
                "Mie",
                "Ju",
                "Vi",
                "Sa"
            ],
            "monthNames": [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "May",
                "Junio",
                "Julio",
                "Augosto",
                "Setiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ],
            "firstDay": 1
        },
        startDate: start,
        endDate: end,
        ranges: {
            'Hoy': [moment(), moment()],
            'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Últimos 7 días': [moment().subtract(6, 'days'), moment()],
            'Últimos 30 días': [moment().subtract(29, 'days'), moment()],
            'Este mes': [moment().startOf('month'), moment().endOf('month')],
            'Último mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);

    $('#btnPrevisualizar').click((e) => {
        e.preventDefault();
        showLoader(true);
        setTimeout(() => {
            showLoader(false);
            previsualizar();
        }, 1000);
    });
});

function previsualizar() {
    console.log(productos);
    data = productos.slice();
    var $containerProductos = $('#productosVendidos');
    var $respuesta = $('#sinResultado');
    $('#contenidoTabla').html('');
    $('#totalTabla').html('');

    // uso de filtros
    var nombreProducto = $('#txtNombreProducto').val();
    var categoria = $('#selectCategoría').val();
    var precioIni = precioInicial;
    var precioFin = precioFinal;
    console.log(categoria);
    data = data.filter(
        producto => producto.nombre.toLowerCase().indexOf(nombreProducto.toLowerCase()) > -1 &&
        producto.precio >= precioIni && producto.precio <= precioFin &&
        producto.categoriaId == (categoria.length > 0 ? categoria : producto.categoriaId)
    );
    console.log(data);

    if (data.length > 0) {
        $containerProductos.show();
        $respuesta.hide();
        var totalVendido = 0;
        $.each(data, function(key, value) {
            totalVendido += (Math.round(value.precio * value.cantidadVendida * 100) / 100);
            $('#contenidoTabla').append(
                '<tr><th class="text-center">' + value.codigo +
                '</th><td class="text-left">' + value.nombre +
                '</td><td class="text-left">' + value.categoria +
                '</td><td class="text-center">' + value.stock +
                '</td><td  class="text-right">' + 'S/. ' + value.precioCompra +
                '</td><td  class="text-right">' + 'S/. ' + value.precio +
                '</td><td class="text-center">' + value.cantidadVendida +
                '</td><td  class="text-right">' + 'S/. ' + (Math.round(value.precio * value.cantidadVendida * 100) / 100) +
                '</td></tr>');
        });
        $('#totalTabla').append(
            '<tr><th colspan="6" class="text-right">' + 'Total vendido' +
            '</th><td colspan="2" class="text-right" id="totalVendido">' + 'S/. ' + totalVendido +
            '</td></tr>');
    } else {
        $containerProductos.hide();
        $respuesta.show();
    }
}

function showLoader(t) {
    if (t) {
        $('#loader').show();
    } else {
        $('#loader').hide();
    }
}